using System;
using Microsoft.Data.Sqlite;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        readonly string _connectionString;
        public CustomerRepository(string connectionString)
        { 
            _connectionString = connectionString;
        }

        public void AddCustomer(Customer customer)
        {
            //Add customer to data source
            using (var connection = new SqliteConnection(_connectionString))
            { 
                connection.Open();

                SqliteCommand command = new SqliteCommand();
                command.Connection = connection;
                command.CommandText = $"INSERT INTO Customers (Id, FullName, Email, Phone) VALUES ({customer.Id}, '{customer.FullName}', '{customer.Email}', '{customer.Phone}')";
                command.ExecuteNonQuery();
            }
        }
    }
}