﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;


namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        readonly string _fileName;

        public XmlParser(string fName)
        { 
            _fileName = fName;  
        }

        public List<Customer> Parse()
        {
            //Parse data
            //List<Customer> newcustomer  = new List<Customer>();
            CustomersList customersList;

            // Deserialize xml data from file
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(CustomersList));

            using (FileStream fs = new FileStream(_fileName, FileMode.OpenOrCreate))
            {
                //Customer[] newcustomerArr = xmlSerializer.Deserialize(fs) as Customer[];
                //newcustomer = (xmlSerializer.Deserialize(fs) as Customer[]).ToList<Customer>();
                customersList = xmlSerializer.Deserialize(fs) as CustomersList;
            }
                       
            return customersList.Customers;
        }
    }
}