﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        
        static void Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            Console.WriteLine("Do you want generate by process or by method [P]/[M]:");
            var reply = Console.ReadLine();
            if (reply == "P")
            {
                ProcessStartInfo procInfo = new ProcessStartInfo();
                // runtime file
                procInfo.FileName = @"D:\DemidovES\OTUS_Education\Lesson_Parallelizm\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Release\netcoreapp3.1\Otus.Teaching.Concurrency.Import.DataGenerator.App";
                // arguments
                procInfo.Arguments = "myXMLfile" + new Random().Next(1, 30);
                Process.Start(procInfo);
            } else
                GenerateCustomersDataFile();

            XmlParser xmlParser = new XmlParser(_dataFilePath);
            
            var loader = new MyDataLoader(xmlParser.Parse(), 100, @"Data Source=..\..\..\..\CustomerDb.db");

            loader.LoadData();
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 1000);
            xmlGenerator.Generate();
        }
    }
}