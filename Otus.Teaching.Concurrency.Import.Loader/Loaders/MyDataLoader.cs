using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class MyDataLoader 
        : IDataLoader 
    {
        ICollection<Customer> _collection;
        int _discret;
        string _connString;

        public MyDataLoader(ICollection<Customer> values, int discret, string connString)
        {
            _collection = values;
            _discret = discret;
            _connString = connString;
        }

        public void LoadData()
        {
            //Console.WriteLine("Loading data...");
            //Thread.Sleep(10000);
            //Console.WriteLine("Loaded data...");

            List<List<Customer>> listAll = new List<List<Customer>>();
            // 
            int threadCount = _collection.Count / _discret;   // count of threads

            if (_collection.Count - threadCount * _discret == 0)
            {
                for (int i = 0; i < threadCount; i++)
                {
                    List<Customer> lst = new List<Customer>();

                    for (int j = i; j < i + _discret; ++j)
                    {
                        lst.Add(_collection.ElementAt(j));
                    }

                    listAll.Add(lst);
                }

                for (int i = 0; i < listAll.Count; i++) 
                {
                    List<Customer> entities = listAll.ElementAt(i);
                    CustomerRepository customerRepository = new CustomerRepository(_connString);

                    //new Thread(() => {

                        for (int j = 0; j < entities.Count; ++j)
                        {
                            customerRepository.AddCustomer(entities.ElementAt(j));
                        }
                    //});
                }
            }
            else if (_collection.Count - threadCount * _discret > 0)
            {
                
            }
            else // all in one thread
            {

            }


        }
    }
}